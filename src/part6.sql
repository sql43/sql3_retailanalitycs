--------------------------------------------------------------------------------
-- Функция, определяющая предложения, ориентированные на кросс-продажи (рост
-- маржи)
--------------------------------------------------------------------------------
--
CREATE
OR REPLACE FUNCTION fnc_part6(
    _number_of_groups INTEGER DEFAULT 1, -- количество групп
    _maximum_churn_index NUMERIC DEFAULT 0, -- максимальный индекс оттока
    _maximum_consumption_stability_index NUMERIC DEFAULT 0, -- максимальный индекс стабильности потребления
    _maximum_share_of_sku NUMERIC DEFAULT 0, -- максимальная доля SKU (в процентах)
    _allowable_margin_share NUMERIC DEFAULT 0 -- допустимая доля маржи (в процентах)
) RETURNS TABLE (
    Customer_ID INTEGER,
    SKU_Name VARCHAR,
    Offer_Discount_Depth
     NUMERIC
) AS $$
BEGIN
    -- Валидируем "Количество групп"
    IF (_number_of_groups < 1) THEN
        RAISE EXCEPTION 'fnc_part6: number of groups must be greater then 0';
    END IF;

    -- Валидируем "максимальная доля SKU (в процентах)"
    IF (_maximum_share_of_sku < 0 OR _maximum_share_of_sku > 100) THEN
        RAISE EXCEPTION 'fnc_part6: maximum share of sku must be in [0,100]';
    END IF;

    -- Валидируем "Допустимая доля маржи (в процентах)"
    IF (_allowable_margin_share < 0 OR _allowable_margin_share > 100) THEN
        RAISE EXCEPTION 'fnc_part6: allowable margin share must be in [0,100]';
    END IF;

    --  Остальное не валидируем, т.к. считаем допустимфм любое значение индекса оттока и индекса стабильности

    _maximum_share_of_sku = _maximum_share_of_sku / 100;
    _allowable_margin_share = _allowable_margin_share / 100;

    RETURN QUERY
        SELECT
            result_data.customer_id AS Customer_ID,
            result_data.sku_name AS SKU_Name,
            ROUND(100 * result_data.min_discount_round) AS Offer_Discount_Depth
        FROM
            (
                SELECT
                    ROW_NUMBER() OVER (
                        PARTITION BY mv_groups.customer_id, -- В рамках каждого покупателя                 
                        mv_groups.group_id -- и каждой группы присваиваем номер товарам по критериям сортировки (чтобы отобрать только первый товар с максимальной маржой)
                        ORDER BY
                            SKU_Retail_Price - SKU_Purchase_Price DESC -- Определение SKU с максимальной маржой
                    ) AS sku_number,
                    DENSE_RANK() OVER (
                        PARTITION BY mv_groups.customer_id -- В рамках каждого покупателя присваиваем ранк группам по критериям сортировки (чтобы далее отобрать N первых групп)
                        ORDER BY
                            Group_Affinity_Index DESC, -- выбирается несколько групп с максимальным индексом востребованности
                            mv_groups.group_id -- на случай, если индекс востребованности одинаковый, чтобы был стабильный результат
                    ) AS group_number,
                    mv_groups.customer_id,
                    CEIL(group_minimum_discount / 0.05) * 0.05 AS min_discount_round,
                    (_allowable_margin_share * (SKU_Retail_Price - SKU_Purchase_Price)) / SKU_Retail_Price AS max_margin_discount,
                    sku.sku_name,
                    SKU_Retail_Price - SKU_Purchase_Price AS margin_abs,
                    mv_sku_transactions_share.share_sku_in_group AS share_sku_in_group
                FROM
                    mv_customers
                    JOIN mv_groups ON mv_groups.customer_id = mv_customers.customer_id
                    JOIN sku ON sku.group_id = mv_groups.group_id
                    JOIN stores ON stores.sku_id = sku.sku_id
                    AND stores.transaction_store_id = mv_customers.customer_primary_store
                    JOIN mv_sku_transactions_share ON mv_sku_transactions_share.sku_id = sku.sku_id
                WHERE
                    Group_Churn_Rate <= _maximum_churn_index -- Индекс оттока по группе не более заданного пользователем значения
                    AND Group_Stability_Index < _maximum_consumption_stability_index -- Индекс стабильности потребления группы составляет менее заданного пользователем значения
                    AND group_minimum_discount > 0 -- Нулевые скидки нас не устраивают (предложение скидки в 0 бессмысленно)
            ) AS result_data
        WHERE
            sku_number = 1 -- отбираем только первый товар (с максимальной маржой)
            AND group_number <= _number_of_groups -- отбираем заданное пользователем количество групп
            AND min_discount_round < max_margin_discount -- исключаем группы, у которых скидка больше допустимой
            AND share_sku_in_group <= _maximum_share_of_sku; -- SKU используется для формирования предложения только в том случае, если доля SKU в группе не превышает заданного пользователем значения

END;
$$ LANGUAGE plpgsql;

--------------------------------------------------------------------------------
-- Проверяем работу функции
--------------------------------------------------------------------------------
-- Пример 1
SELECT
    *
FROM
    fnc_part6(
        _number_of_groups := 4,
        _maximum_churn_index := 0.5,
        _maximum_consumption_stability_index := 0.5,
        _maximum_share_of_sku := 100,
        _allowable_margin_share := 70
    );

-- Пример 2
SELECT
    *
FROM
    fnc_part6(
        _number_of_groups := 4,
        _maximum_churn_index := 1.5,
        _maximum_consumption_stability_index := 1.5,
        _maximum_share_of_sku := 100,
        _allowable_margin_share := 80
    );

-- Пример 3
SELECT
    *
FROM
    fnc_part6(
        _number_of_groups := 1,
        _maximum_churn_index := 1.5,
        _maximum_consumption_stability_index := 1.5,
        _maximum_share_of_sku := 100,
        _allowable_margin_share := 80
    );