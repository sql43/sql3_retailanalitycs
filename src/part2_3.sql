--------------------------------------------------------------------------------
-- Part 2. Создание представлений
--------------------------------------------------------------------------------
-- Представление Периоды
--------------------------------------------------------------------------------
-- блок: temp_base:
--       first_group_purchase_date: определяем дату первой покупки определенной группы для каждого клиента
--       last_group_purchase_date: определяем дату последней покупки определенной группы для каждого клиента
--       group_purchase: определение количества транзакций с анализируемой группой для определенного клиента
--                       DISTINCT можно было не писать так как в одной транзакции группы не повторяются. Например,
--                       если в одной транзации есть товары принадлежащие к одной группе, то они суммируются в одну
--                       группу.
--       group_min_discount: Нужно предоставленную скидку поделить на всю сумму, которую клиент должен заплить без
--                           учета скидки. И узнаем долю предоставленной скидки от общей суммы.
-- блок: temp_final:
--       group_frequency: Узнаем через сколько дней обычно клиент покупает определенный товар. Частота покупки
--                        определенной группы в днях. Например, раз в 3 дня или раз в 100 дней.
--------------------------------------------------------------------------------
-- 
--------------------------------------------------------------------------------
-- Функция для получения данных для представления mv_periods
--------------------------------------------------------------------------------
--
CREATE
OR REPLACE FUNCTION fnc_create_mv_periods() RETURNS TABLE (
    customer_id_ INTEGER,
    group_id_ INTEGER,
    first_group_purchase_date_ TIMESTAMP,
    last_group_purchase_date_ TIMESTAMP,
    group_purchase_ INTEGER,
    group_frequency_ NUMERIC,
    group_min_discount_ NUMERIC
) AS $$
DECLARE
    first_date_ TIMESTAMP := fnc_first_date();
    last_date_ TIMESTAMP := fnc_last_date();

BEGIN
    RETURN QUERY
        WITH temp_base AS (
            SELECT
                ph.customer_id,
                ph.group_id,
                MIN(transaction_datetime) AS first_group_purchase_date,
                MAX(transaction_datetime) AS last_group_purchase_date,
                COUNT(DISTINCT ph.transaction_id) :: INTEGER AS group_purchase,
                -- Переводим "0" в NULL. Так возьмем мин не нуль. Если будут все NULL вставим "0"
                COALESCE(MIN(NULLIF(group_min_discount, 0)), 0) AS group_min_discount
            FROM
                mv_purchase_history AS ph
                LEFT JOIN (
                    SELECT
                        customer_id,
                        checks.transaction_id,
                        group_id,
                        sku_discount / sku_summ AS group_min_discount
                    FROM
                        checks
                        INNER JOIN sku ON checks.sku_id = sku.sku_id
                        INNER JOIN transactions ON transactions.transaction_id = checks.transaction_id
                        INNER JOIN cards ON cards.customer_card_id = transactions.customer_card_id
                ) AS temp_1 ON ph.customer_id = temp_1.customer_id
                AND ph.transaction_id = temp_1.transaction_id
                AND ph.group_id = temp_1.group_id
            WHERE
                transaction_datetime BETWEEN first_date_
                AND last_date_
            GROUP BY
                ph.customer_id,
                ph.group_id
        ),
        temp_final AS (
            SELECT
                customer_id,
                group_id,
                first_group_purchase_date,
                last_group_purchase_date,
                group_purchase,
                (
                    last_group_purchase_date - first_group_purchase_date + INTERVAL '1' DAY
                ) / group_purchase AS group_frequency,
                group_min_discount
            FROM
                temp_base
        )
        SELECT
            customer_id,
            group_id,
            first_group_purchase_date,
            last_group_purchase_date,
            group_purchase,
            EXTRACT(
                EPOCH
                FROM
                    group_frequency
            ) / 86400.0 AS group_frequency,
            group_min_discount
        FROM
            temp_final;

END;
$$ LANGUAGE plpgsql;

--------------------------------------------------------------------------------
-- Процедура для создания представления mv_periods
--------------------------------------------------------------------------------
--
CREATE
OR REPLACE PROCEDURE pr_create_mv_periods() AS $$
BEGIN
    DROP MATERIALIZED VIEW IF EXISTS mv_periods;
    CREATE MATERIALIZED VIEW mv_periods AS
        SELECT
            customer_id_ AS customer_id,
            group_id_ AS group_id,
            first_group_purchase_date_ AS first_group_purchase_date,
            last_group_purchase_date_ AS last_group_purchase_date,
            group_purchase_ AS group_purchase,
            group_frequency_ AS group_frequency,
            group_min_discount_ AS group_min_discount
        FROM
            fnc_create_mv_periods();

END;
$$ LANGUAGE plpgsql;

--------------------------------------------------------------------------------
-- Создаем представление mv_periods
--------------------------------------------------------------------------------
--
CALL pr_create_mv_periods();

--------------------------------------------------------------------------------
-- Проверка view
--------------------------------------------------------------------------------
--
-- Смотрим все данные mv
SELECT
    *
FROM
    mv_periods;

-- Смотрим топ10 записей
SELECT
    *
FROM
    mv_periods
LIMIT
    10;

-- Смотрим записи покупателя с ID = 3
SELECT
    *
FROM
    mv_periods
WHERE
    customer_id = 3;

-- А что тут происходит, сейчас расскажет Денис aka bastionm
DO $$
BEGIN
    RAISE NOTICE 'Check_1 When there is no discount must be = 0:';

    IF (
        SELECT
            group_min_discount
        FROM
            mv_periods
        WHERE
            customer_id = 1
            AND group_id = 1
    ) = 0 THEN
        RAISE NOTICE '--------TRUE';
    ELSE
        RAISE NOTICE '--------FALSE';
    END IF;

    RAISE NOTICE 'Check_2 all group_frequency must be >= 1:';

    IF (
        SELECT
            DISTINCT group_frequency
        FROM
            mv_periods
        WHERE
            group_frequency = 0
    ) = 0 THEN
        RAISE NOTICE '--------FALSE';
    ELSE
        RAISE NOTICE '--------TRUE';

    END IF;

END;
$$ LANGUAGE plpgsql