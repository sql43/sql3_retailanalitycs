--------------------------------------------------------------------------------
-- Part 2. Создание представлений
--------------------------------------------------------------------------------
-- Представление Клиенты
--------------------------------------------------------------------------------
-- блок: temp_pre_base: необходим чтобы найти идентификатор основного магазина (Customer_Primary_Store)
--       next_store_order_by_date:  используем оконную функцию LEAD. Сначала выделим группы "customer_id". Далле
--                                  отсортируем данные по дате "transaction_datetime" от самого позднее до самого
--                                  раннего. И сдвинем колонку  "transaction_store_id" на 1 строку вверх, т.е.
--                                  возьмется следующее значение. У последнего знчения не будет следующего, поэтому NULL
--       double_next_store_order_by_date:  2-oe следующее значение относитльно "transaction_store_id".
--
--       У нас  "есть transaction_store_id", "next_store_order_by_date", "double_next_store_order_by_date" - можем
--       проверить три последние транзакции у определенного клиента совершены в одном и том же магазине или нет.
--
--       last_three_transc_in_one_store: false - три последние транзакции у совершены не в одном и том же магазине.
--                                       true - три последние транзакции у совершены в одном и том же магазине.
--       count_trans_custom_in_one_store: сколько транзакицй было совершено данным клинетом в данном магазине.
--
--       Если "last_three_transc_in_one_store" - false, то группируем по customer_id и сортируем по
--       "count_trans_custom_in_one_store", "transaction_datetime" и берем первое значение и сохраняем в
--       "Customer_Primary_Store".
--
-- блок: temp_base: здесь мы явно группируем по "customer_id" подготавливаем данные для дальнейшего разделения
--                  по 3-м сегментам.
--       customer_average_check: Суммируются все транзакции по всем картам каждого клиента по полю Transaction_Summ
--                               таблицы Транзакции, после чего полученная сумма делится на количество транзакций.
--       customer_frequency: берется последняя дата посщения и первая дата посещения и делится на количество посещений
--                           в промежутке этих дат. Получатся интенсивность посещений.
--       customer_inactive_period: период бездействия клинета. Т.е. берется дата когда был выгружен данный отчет из
--       таблице "date_of_analysis_formation" и вычитаем последнюю дату посещения (имеется ввиду транзакция, покупка).
-- блок: temp_row_number: заканчиваем подготовку данных для для дальнейшего разделения по 3-м сегментам.
--       desk_queue_average_check: идет обычная нумерация строк от 1-го до конца.  Оконная цункция Row_number сначала
--                                 сортирует можно сказать по значениям столбца customer_average_check и нумерует
--                                 строки. Это необходимо для дальнейшего рассчета "Сегмент по среднему чеку"
--                                 (customer_average_check_segment).
--                                 DESC NULLS LAST - При сортировки от большего к меньшему необходимо, чтобы NULL были
--                                                   в конце таблице. Поэтому пишем NULLS LAST.
--       desk_queue_frequency: также идет нумерация строк, если бы колонка "customer_frequency" была отсортированна от
--                             большего к меньшему.
--
-- блок: temp_segment_assignment: сегментируем данные на группы.
--       customer_average_check_segment: Первые 10% значений по отсортированному столбцу desk_queue_average_check -
--                                       это "High". Далле от 10% до 25% - это "Medium". Остальное - это "Low".
--                                       (SELECT COUNT(*) FROM personal_data) - это  сколько всего строк.
--                                       Также у нас есть столбец с нумерованными строками. Остается номер каждой
--                                       строки поделить на общее количество строк и узнать к какой группе относится
--                                       каждый покупатель.
--       customer_frequency_segment: Первые 10% значений по отсортированному столбцу desk_queue_frequency - это
--                                   "High". Далле от 10% до 25% - это "Medium". Остальное - это "Low".
--       customer_churn_rate: По сути (не активность дней клиента) деленная (через сколько дней обычно приходит клиент).
--                            Получаем чем больше число, тем больше вероятность,что клиент больше не придет.
--
-- Заметки:
-- 1) Все округляется до десятичной дроби, как указано в формате значений таблицы.
--------------------------------------------------------------------------------
--
--------------------------------------------------------------------------------
-- Функция для получения даты начала анализируемого периода (исходя из данных
-- таблицы date_of_analysis_formation)
--------------------------------------------------------------------------------
--
CREATE
OR REPLACE FUNCTION fnc_first_date() RETURNS TIMESTAMP AS $$
BEGIN
    IF (
        SELECT
            COUNT(*)
        FROM
            date_of_analysis_formation
    ) > 1 THEN RETURN (
        SELECT
            analysis_formation
        FROM
            (
                SELECT
                    analysis_formation
                FROM
                    date_of_analysis_formation
                ORDER BY
                    analysis_formation DESC
                LIMIT
                    2
            ) AS temp_1
        ORDER BY
            analysis_formation ASC
        LIMIT
            1
    );

    ELSE RETURN (
        SELECT
            DISTINCT FIRST_VALUE(transaction_datetime) OVER wnd1
        FROM
            transactions WINDOW wnd1 AS (
                ORDER BY
                    transaction_datetime
            )
    );

    END IF;

END;
$$ LANGUAGE plpgsql;

--------------------------------------------------------------------------------
-- Функция для получения даты конца анализируемого периода (исходя из данных
-- таблицы date_of_analysis_formation)
--------------------------------------------------------------------------------
--
CREATE
OR REPLACE FUNCTION fnc_last_date() RETURNS TIMESTAMP AS $$
BEGIN
    IF (
        SELECT
            COUNT(*)
        FROM
            date_of_analysis_formation
    ) > 0 THEN RETURN (
        SELECT
            analysis_formation
        FROM
            date_of_analysis_formation
        ORDER BY
            analysis_formation DESC
        LIMIT
            1
    );

    ELSE RETURN (
        SELECT
            transaction_datetime
        FROM
            transactions
        ORDER BY
            transaction_datetime DESC
        LIMIT
            1
    );

    END IF;

END;
$$ LANGUAGE plpgsql;

--------------------------------------------------------------------------------
-- Функция для получения данных для представления mv_customers
--------------------------------------------------------------------------------
--
CREATE
OR REPLACE FUNCTION fnc_create_mv_customers() RETURNS TABLE (
    customer_id_ INTEGER,
    customer_average_check_ NUMERIC,
    customer_average_check_segment_ VARCHAR,
    customer_frequency_ FLOAT,
    customer_frequency_segment_ VARCHAR,
    customer_inactive_period_ FLOAT,
    customer_churn_rate_ FLOAT,
    customer_churn_segment_ VARCHAR,
    customer_segment_ INTEGER,
    customer_primary_store_ INTEGER
) AS $$
DECLARE
    first_date_ TIMESTAMP := fnc_first_date();
    last_date_ TIMESTAMP := fnc_last_date();
BEGIN
    RETURN QUERY
        WITH temp_pre_base AS (
            SELECT
                *,
                CASE
                    WHEN (
                        FIRST_VALUE(last_three_transc_in_one_store) OVER(PARTITION BY customer_id)
                    ) IS FALSE THEN FIRST_VALUE(transaction_store_id) OVER(
                        PARTITION BY customer_id
                        ORDER BY
                            count_trans_custom_in_one_store DESC,
                            transaction_datetime DESC
                    )
                    ELSE FIRST_VALUE(transaction_store_id) OVER(PARTITION BY customer_id)
                END AS customer_primary_store
            FROM
                (
                    SELECT
                        *,
                        CASE
                            WHEN (
                                FIRST_VALUE(transaction_store_id) OVER(PARTITION BY customer_id)
                            ) IS NULL
                            OR (
                                FIRST_VALUE(next_store_order_by_date) OVER(PARTITION BY customer_id)
                            ) IS NULL
                            OR (
                                FIRST_VALUE(double_next_store_order_by_date) OVER(PARTITION BY customer_id)
                            ) IS NULL THEN FALSE
                            WHEN (
                                FIRST_VALUE(transaction_store_id) OVER(PARTITION BY customer_id)
                            ) = (
                                FIRST_VALUE(next_store_order_by_date) OVER(PARTITION BY customer_id)
                            )
                            AND (
                                FIRST_VALUE(next_store_order_by_date) OVER(PARTITION BY customer_id)
                            ) = (
                                FIRST_VALUE(double_next_store_order_by_date) OVER(PARTITION BY customer_id)
                            ) THEN TRUE
                            ElSE FALSE
                        END AS last_three_transc_in_one_store
                    FROM
                        (
                            SELECT
                                *,
                                LEAD(next_store_order_by_date) OVER(
                                    PARTITION BY customer_id
                                    ORDER BY
                                        transaction_datetime DESC
                                ) double_next_store_order_by_date,
                                COUNT(transaction_store_id) OVER(PARTITION BY customer_id, transaction_store_id) count_trans_custom_in_one_store
                            FROM
                                (
                                    SELECT
                                        customer_id,
                                        transaction_store_id,
                                        transaction_datetime,
                                        transaction_summ,
                                        LEAD(transaction_store_id) OVER(
                                            PARTITION BY customer_id
                                            ORDER BY
                                                transaction_datetime DESC
                                        ) next_store_order_by_date
                                    FROM
                                        cards AS c
                                        JOIN transactions AS t ON c.customer_card_id = t.customer_card_id
                                    WHERE
                                        transaction_datetime BETWEEN first_date_
                                        AND last_date_
                                ) AS tabl_1
                        ) AS tabl_2
                ) tabl_3
        ),
        temp_base AS (
            SELECT
                customer_id,
                AVG(transaction_summ) AS customer_average_check,
                EXTRACT(
                    EPOCH
                    FROM
                        MAX(transaction_datetime) - MIN(transaction_datetime)
                ) :: FLOAT / 86400.0 / COUNT(*) AS customer_frequency,
                EXTRACT(
                    EPOCH
                    FROM
                        fnc_last_date() - MAX(transaction_datetime)
                ) :: FLOAT / 86400.0 AS customer_inactive_period,
                (SUM(customer_primary_store) / COUNT(*)) :: INTEGER AS customer_primary_store
            FROM
                temp_pre_base
            GROUP BY
                customer_id
            HAVING
                COUNT(*) > 1
        ),
        temp_row_number AS (
            SELECT
                customer_id,
                customer_average_check,
                customer_inactive_period,
                customer_frequency,
                row_number() OVER(
                    ORDER BY
                        customer_average_check DESC
                ) AS desk_queue_average_check,
                row_number() OVER(
                    ORDER BY
                        customer_frequency
                ) AS desk_queue_frequency,
                customer_primary_store
            FROM
                temp_base
        ),
        temp_segment_assignment AS (
            SELECT
                customer_id,
                customer_average_check,
                CASE
                    WHEN desk_queue_average_check :: NUMERIC / (
                        SELECT
                            COUNT(*)
                        FROM
                            temp_row_number
                    ) <= 0.1 THEN 'High' :: VARCHAR
                    WHEN (
                        desk_queue_average_check :: NUMERIC / (
                            SELECT
                                COUNT(*)
                            FROM
                                temp_row_number
                        )
                    ) <= 0.35 THEN 'Medium' :: VARCHAR
                    ELSE 'Low' :: VARCHAR
                END AS customer_average_check_segment,
                customer_frequency,
                CASE
                    WHEN desk_queue_frequency :: NUMERIC / (
                        SELECT
                            COUNT(*)
                        FROM
                            temp_row_number
                    ) <= 0.1 THEN 'High' :: VARCHAR
                    WHEN desk_queue_frequency :: NUMERIC / (
                        SELECT
                            COUNT(*)
                        FROM
                            temp_row_number
                    ) <= 0.35 THEN 'Medium' :: VARCHAR
                    ELSE 'Low' :: VARCHAR
                END AS customer_frequency_segment,
                customer_inactive_period,
                (customer_inactive_period / customer_frequency) AS customer_churn_rate,
                CASE
                    WHEN (customer_inactive_period / customer_frequency) >= 0
                    AND (customer_inactive_period / customer_frequency) <= 2 THEN 'Low' :: VARCHAR
                    WHEN (customer_inactive_period / customer_frequency) > 2
                    AND (customer_inactive_period / customer_frequency) <= 5 THEN 'Medium' :: VARCHAR
                    ELSE 'High' :: VARCHAR
                END AS customer_churn_segment,
                customer_primary_store
            FROM
                temp_row_number
        )
        SELECT
            customer_id,
            customer_average_check,
            customer_average_check_segment,
            customer_frequency,
            customer_frequency_segment,
            customer_inactive_period,
            customer_churn_rate,
            customer_churn_segment,
            segment_number.segment AS customer_segment,
            customer_primary_store
        FROM
            temp_segment_assignment
            LEFT JOIN segment_number ON temp_segment_assignment.customer_average_check_segment = segment_number.average_check
            AND temp_segment_assignment.customer_frequency_segment = segment_number.frequency_of_purchases
            AND temp_segment_assignment.customer_churn_segment = segment_number.churn_probability;

END;
$$ LANGUAGE plpgsql;

--------------------------------------------------------------------------------
-- Процедура для создания представления mv_customers
--------------------------------------------------------------------------------
--
CREATE
OR REPLACE PROCEDURE pr_create_mv_customers() AS $$
BEGIN
    -- Пересоздание представления mv_customers
    DROP MATERIALIZED VIEW IF EXISTS mv_customers;

    CREATE MATERIALIZED VIEW mv_customers AS
    SELECT
        customer_id_ AS customer_id,
        customer_average_check_ AS customer_average_check,
        customer_average_check_segment_ AS customer_average_check_segment,
        customer_frequency_ AS customer_frequency,
        customer_frequency_segment_ AS customer_frequency_segment,
        customer_inactive_period_ AS customer_inactive_period,
        customer_churn_rate_ AS customer_churn_rate,
        customer_churn_segment_ AS customer_churn_segment,
        customer_segment_ AS customer_segment,
        customer_primary_store_ AS customer_primary_store
    FROM
        fnc_create_mv_customers();

    -- Полезный индекс для ускорения работы с mv
    CREATE INDEX IF NOT EXISTS idx_mv_customers_customer_id ON mv_customers USING btree(customer_id);

END;
$$ LANGUAGE plpgsql;

--------------------------------------------------------------------------------
-- Создаем представление mv_customers
--------------------------------------------------------------------------------
--
CALL pr_create_mv_customers();

--------------------------------------------------------------------------------
-- Проверка view
--------------------------------------------------------------------------------
--
-- Смотрим все данные mv
SELECT
    *
FROM
    mv_customers;
	
-- Смотрим только записи, относящиеся к верхнему сегменту по среднему чеку
SELECT
    *
FROM
    mv_customers
WHERE
    customer_average_check_segment = 'High';
	
-- Смотрим только записи, с частотой визитов более 100
SELECT
    *
FROM
    mv_customers
WHERE
    customer_frequency > 100;

-- А что тут происходит, сейчас расскажет Денис aka bastionm
DO $$
BEGIN
    --  CHECK № 1
    IF (
        SELECT
            customer_primary_store
        FROM
            mv_customers
        WHERE
            customer_id = 1
    ) = 11 THEN
        RAISE NOTICE 'Check_1 customer_primary_store when there is not three transaction in one store and';
        RAISE NOTICE '------- there are two stores with the same share of transactions: TRUE';
    ELSE
        RAISE NOTICE 'Check_1 customer_primary_store: FALSE';
    END IF;

    --  CHECK № 2
    IF (
        SELECT
            customer_segment
        FROM
            mv_customers
        WHERE
            customer_id = 169
    ) = 3 THEN
        RAISE NOTICE 'Check_2 customer_segment when customer buy nothing: TRUE';
    ELSE
        RAISE NOTICE 'Check_2 customer_segment when customer buy nothing: FALSE';
    END IF;

    --  CHECK № 3
    IF ROUND(
        (
            SELECT
                customer_inactive_period
            FROM
                mv_customers
            WHERE
                customer_id = 148
        ) :: numeric / (
            SELECT
                customer_frequency
            FROM
                mv_customers
            WHERE
                customer_id = 148
        ) :: numeric,
        1
    ) = 1.9 THEN
        RAISE NOTICE 'Check_3 customer_churn_rate calculation: TRUE';
    ELSE
        RAISE NOTICE 'Check_3 customer_churn_rate calculation: FALSE';
    END IF;

END;
$$ LANGUAGE plpgsql;