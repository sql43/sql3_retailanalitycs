--------------------------------------------------------------------------------
-- Part 3. Ролевая модель
--------------------------------------------------------------------------------
-- Администратор имеет полные права на редактирование и просмотр
-- любой информации, запуск и остановку процесса обработки.
--
-- Посетитель имеет право только просмотр информации из всех таблиц.
--------------------------------------------------------------------------------
-- g_read_all_data: Read all data (tables, views, sequences)
-- pg_write_all_data: Write all data (tables, views, sequences)
--------------------------------------------------------------------------------

CREATE ROLE administrator LOGIN PASSWORD '1234';
CREATE ROLE visitor LOGIN PASSWORD '123';

GRANT ALL PRIVILEGES ON ALL SEQUENCES IN SCHEMA public TO administrator;
GRANT ALL PRIVILEGES ON ALL TABLES IN SCHEMA public TO administrator;

GRANT SELECT ON ALL TABLES IN SCHEMA public TO visitor;

-- Права на БД не выдаем, т.к. согласно документации на PG:
--
-- PostgreSQL grants default privileges on some types of objects to PUBLIC. No
-- privileges are granted to PUBLIC by default on tables, columns, schemas or 
-- tablespaces. For other types, the default privileges granted to PUBLIC are as 
-- follows: CONNECT and CREATE TEMP TABLE for databases;
--
-- Если очень хочется сначала удалить все права по умолчанию, то делаем:
-- REVOKE ALL PRIVILEGES ON DATABASE <database> FROM public;
--------------------------------------------------------------------------------
-- По умолчанию в PG авторизация по паролю выключена, поэтому при авторизации
-- пароль не спросит. Если надо включить:
-- 1) Открываем файл pg_hba.conf 
-- (у нас в /Users/hubertfu/Library/Application \Support/Postgres/var-14/)
-- 2) Метод авторизации указываем md5
--------------------------------------------------------------------------------
-- 
-- Коннектимся как administrator:
-- psql -Uadministrator -dsql3_hubertfu_and_bastionm
SELECT * FROM personal_data; -- OK
SELECT * FROM mv_customers; -- OK

INSERT INTO personal_data VALUES (DEFAULT, 'test','test','test@test.ru','+7900000000'); -- OK
SELECT * FROM personal_data; -- OK
DELETE FROM personal_data WHERE customer_id = (SELECT MAX(customer_id) FROM personal_data); -- OK
SELECT * FROM personal_data; -- OK

-- 
-- Коннектимся как visitor:
-- psql -Uvisitor -dsql3_hubertfu_and_bastionm
SELECT * FROM personal_data; -- OK
SELECT * FROM mv_customers; -- OK

INSERT INTO personal_data VALUES (DEFAULT, 'test','test','test@test.ru','+7900000000'); -- FAIL
SELECT * FROM personal_data; -- OK
DELETE FROM personal_data WHERE customer_id = (SELECT MAX(customer_id) FROM personal_data); -- FAIL
SELECT * FROM personal_data; -- OK

-- Подропаем, что создали (если надо)
-- REASSIGN OWNED BY administrator TO postgres;
-- DROP OWNED BY administrator;
-- DROP ROLE administrator;
-- REASSIGN OWNED BY visitor TO postgres;
-- DROP OWNED BY visitor;
-- DROP ROLE visitor;