--------------------------------------------------------------------------------
-- Part 2. Создание представлений
--------------------------------------------------------------------------------
-- Представление Группы
--------------------------------------------------------------------------------
-- блок: temp_pre_base: расчет индекса стабильности (group_stability_index)
--       У нас средний показатель частоты покупки опредленной группы определенного клиента. И нам нужно
--       Сравнить с каждым интервалом по отдельности со средним. Чем ближе к 1 - слабый разброс.
--       Дальше от 1 - разбрс сильный. Дргуими словами считаем дисперсию.
--                 | Каждый интервал покупки группы - средний интервал покупки группы |
--                 |            -------------------------------- (делим)              | -> для каждого клиента
--                 |              средний интервал покупки группы                     |
-- блок: temp_base: group_affinity_index и group_churn_rate
--       group_affinity_index - количество транзакции определейнной группы и клиента /
--                              общее количество транзакций клиента
-- блок: temp_margin: расчет маржы (group_summ_paid - group_cost)
--------------------------------------------------------------------------------
--
--------------------------------------------------------------------------------
-- Функция для получения данных для представления mv_groups
--------------------------------------------------------------------------------
--

CREATE
OR REPLACE FUNCTION fnc_create_v_groups(
    IN flag INTEGER DEFAULT 1,
    IN days_interv INTERVAL DEFAULT '6000 days' :: INTERVAL,
    IN count_transcaions INTEGER DEFAULT 200
) RETURNS TABLE (
    customer_id_ INTEGER,
    group_id_ INTEGER,
    group_affinity_index_ NUMERIC,
    group_churn_rate_ NUMERIC,
    group_stability_index_ NUMERIC,
    group_margin_ NUMERIC,
    group_discount_share NUMERIC,
    group_minimum_discount NUMERIC,
    group_average_discount_ NUMERIC
) AS $$
DECLARE
    first_date_ TIMESTAMP := fnc_first_date();
    last_date_ TIMESTAMP := fnc_last_date();
BEGIN
    RETURN QUERY
        WITH pre_temp_base_0 AS (
            SELECT
                vp_customer_id,
                vp_group_id,
                SUM(bool_group_purchase) AS all_group_puchase
            FROM
                (
                    SELECT
                        vph.customer_id AS vph_customer_id,
                        vph.group_id AS vph_group_id,
                        transaction_datetime,
                        vp.customer_id AS vp_customer_id,
                        vp.group_id AS vp_group_id,
                        first_group_purchase_date,
                        last_group_purchase_date,
                        CASE
                            WHEN transaction_datetime BETWEEN first_group_purchase_date
                            AND last_group_purchase_date THEN 1
                            ELSE 0
                        END AS bool_group_purchase
                    FROM
                        (
                            SELECT
                                customer_id,
                                group_id,
                                transaction_id,
                                transaction_datetime,
                                COALESCE(
                                    LEAD(transaction_datetime) OVER(
                                        PARTITION BY customer_id,
                                        group_id
                                        ORDER BY
                                            transaction_datetime
                                    ),
                                    transaction_datetime
                                ) AS next_datetime
                            FROM
                                mv_purchase_history
                            ORDER BY
                                customer_id,
                                group_id,
                                transaction_datetime
                        ) as vph
                        CROSS JOIN mv_periods vp
                    WHERE
                        vph.customer_id = vp.customer_id
                ) AS tmp_1
            GROUP BY
                vp_customer_id,
                vp_group_id
        ),
        pre_temp_base AS (
            SELECT
                temp_1.customer_id,
                temp_1.group_id,
                AVG(
                    ABS(
                        (
                            EXTRACT(
                                EPOCH
                                FROM
                                    (next_datetime - transaction_datetime)
                            ) / 86400.0 - group_frequency
                        )
                    ) / group_frequency
                ) AS group_stability_index,
                SUM(all_group_puchase) / COUNT(*) AS all_group_puchase
            FROM
                (
                    SELECT
                        customer_id,
                        group_id,
                        transaction_id,
                        transaction_datetime,
                        COALESCE(
                            LEAD(transaction_datetime) OVER(
                                PARTITION BY customer_id,
                                group_id
                                ORDER BY
                                    transaction_datetime
                            ),
                            transaction_datetime
                        ) AS next_datetime
                    FROM
                        mv_purchase_history
                    ORDER BY
                        customer_id,
                        group_id,
                        transaction_datetime
                ) as temp_1
                LEFT JOIN mv_periods ON mv_periods.customer_id = temp_1.customer_id
                AND mv_periods.group_id = temp_1.group_id
                LEFT JOIN pre_temp_base_0 ON pre_temp_base_0.vp_customer_id = temp_1.customer_id
                AND pre_temp_base_0.vp_group_id = temp_1.group_id
            GROUP BY
                temp_1.customer_id,
                temp_1.group_id
        ),
        temp_base AS (
            SELECT
                mv_periods.customer_id,
                mv_periods.group_id,
                last_group_purchase_date,
                group_purchase,
                group_frequency,
                group_purchase / all_group_puchase AS group_affinity_index,
                (last_date_ - last_group_purchase_date) AS group_churn_rate,
                group_stability_index
            FROM
                mv_periods
                JOIN pre_temp_base ON mv_periods.customer_id = pre_temp_base.customer_id
                AND mv_periods.group_id = pre_temp_base.group_id
        ),
        temp_margin AS (
            SELECT
                temp_base.customer_id,
                temp_base.group_id,
                group_affinity_index,
                EXTRACT(
                    EPOCH
                    FROM
                        group_churn_rate
                ) / 86400.0 / group_frequency AS group_churn_rate,
                group_stability_index,
                group_average_discount,
                group_margin
            FROM
                temp_base
                JOIN (
                    SELECT
                        customer_id,
                        group_id,
                        SUM(group_summ_paid) / SUM(group_summ) AS group_average_discount,
                        CASE
                            WHEN (flag = 1) THEN SUM(group_summ_paid - group_cost) FILTER(
                                WHERE
                                    transaction_datetime BETWEEN last_date_ - days_interv
                                    AND last_date_
                            )
                            WHEN (flag = 2) THEN (
                                SELECT
                                    SUM(differ_cost)
                                FROM
                                    (
                                        SELECT
                                            customer_id,
                                            transaction_id,
                                            group_id,
                                            group_summ_paid - group_cost AS differ_cost
                                        FROM
                                            mv_purchase_history
                                        ORDER BY
                                            transaction_datetime DESC
                                        LIMIT
                                            count_transcaions
                                    ) AS buff
                                WHERE
                                    mv_purchase_history.customer_id = buff.customer_id
                                    AND mv_purchase_history.group_id = buff.group_id
                            )
                        END AS group_margin
                    FROM
                        mv_purchase_history
                    GROUP BY
                        customer_id,
                        group_id
                ) AS asd ON temp_base.customer_id = asd.customer_id
                AND temp_base.group_id = asd.group_id
        )
        SELECT
            buff.customer_id,
            buff.group_id,
            group_affinity_index,
            group_churn_rate,
            group_stability_index,
            group_margin,
            count_trans_with_discount :: NUMERIC / group_purchase :: NUMERIC AS group_discount_share,
            group_min_discount,
            group_average_discount
        FROM
            (
                SELECT
                    customer_id,
                    group_id,
                    COUNT(*) AS count_trans_with_discount
                FROM
                    checks
                    JOIN transactions t ON checks.transaction_id = t.transaction_id
                    JOIN cards c on t.customer_card_id = c.customer_card_id
                    JOIN sku s on checks.sku_id = s.sku_id
                WHERE
                    sku_discount != 0
                    AND transaction_datetime BETWEEN first_date_
                    AND last_date_
                GROUP BY
                    customer_id,
                    group_id
            ) AS buff
            JOIN mv_periods ON mv_periods.customer_id = buff.customer_id
            AND mv_periods.group_id = buff.group_id
            JOIN temp_margin ON temp_margin.customer_id = buff.customer_id
            AND temp_margin.group_id = buff.group_id;

END;
$$ LANGUAGE plpgsql;

--------------------------------------------------------------------------------
-- Процедура для создания представления mv_groups
--------------------------------------------------------------------------------
--
CREATE
OR REPLACE PROCEDURE pr_create_mv_groups() AS $$
BEGIN
    DROP MATERIALIZED VIEW IF EXISTS mv_groups;
    CREATE MATERIALIZED VIEW mv_groups AS
    SELECT
        customer_id_ AS customer_id,
        group_id_ AS group_id,
        group_affinity_index_ AS group_affinity_index,
        group_churn_rate_ AS group_churn_rate,
        group_stability_index_ AS group_stability_index,
        group_margin_ AS group_margin,
        group_discount_share AS group_discount_share,
        group_minimum_discount AS group_minimum_discount,
        group_average_discount_ AS group_average_discount
    FROM
--      Параметр flag, days_interv, count_transcaions  берем из input_settings
        fnc_create_v_groups((SELECT
            CASE
                WHEN (SELECT (SELECT set_value FROM input_settings
                              WHERE key_setting_name = 'mv_groups_flag')) IS  NOT NULL
                THEN (SELECT set_value FROM input_settings
                      WHERE key_setting_name = 'mv_groups_flag')::INTEGER
                ELSE 1::INTEGER
            END), (SELECT
            CASE
                WHEN (SELECT (SELECT set_value FROM input_settings
                              WHERE key_setting_name = 'mv_groups_days_interv')) IS  NOT NULL
                THEN (SELECT set_value FROM input_settings
                      WHERE key_setting_name = 'mv_groups_days_interv')::INTERVAL
                ELSE '6000 days' :: INTERVAL
            END), (SELECT
            CASE
                WHEN (SELECT (SELECT set_value FROM input_settings
                              WHERE key_setting_name = 'mv_groups_count_transcaions')) IS  NOT NULL
                THEN (SELECT set_value FROM input_settings
                      WHERE key_setting_name = 'mv_groups_count_transcaions')::INTEGER
                ELSE 200::INTEGER
            END));
    -- Полезные индексы для ускорения работы с mv
    CREATE INDEX IF NOT EXISTS idx_mv_groups_customer_id ON mv_groups USING btree(customer_id);
    CREATE INDEX IF NOT EXISTS idx_mv_groups_group_id ON mv_groups USING btree(group_id);
    CREATE INDEX IF NOT EXISTS idx_mv_groups_group_stability_index ON mv_groups USING btree(group_stability_index);
    CREATE INDEX IF NOT EXISTS idx_mv_groups_group_churn_rate ON mv_groups USING btree(group_churn_rate);

END;
$$ LANGUAGE plpgsql;

--------------------------------------------------------------------------------
-- Создаем представление mv_customers
--------------------------------------------------------------------------------
--
CALL pr_create_mv_groups();

--------------------------------------------------------------------------------
-- Проверка view
--------------------------------------------------------------------------------
--
-- Посмотрим все записи
SELECT
    *
FROM
    mv_groups;

SELECT
    *
FROM
    fnc_create_v_groups(1);

-- Смотрим топ10 записей
SELECT
    *
FROM
    mv_groups
LIMIT
    10;

-- Смотрим записи покупателя с ID = 3
SELECT
    *
FROM
    mv_groups
WHERE
    customer_id = 3;