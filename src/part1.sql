-- Блок для удаления всех таблиц
DROP TABLE IF EXISTS date_of_analysis_formation CASCADE;
DROP TABLE IF EXISTS checks CASCADE;
DROP TABLE IF EXISTS transactions CASCADE;
DROP TABLE IF EXISTS stores CASCADE;
DROP TABLE IF EXISTS sku CASCADE;
DROP TABLE IF EXISTS groups_sku CASCADE;
DROP TABLE IF EXISTS cards CASCADE;
DROP TABLE IF EXISTS personal_data CASCADE;
DROP TABLE IF EXISTS input_settings CASCADE;
DROP TABLE IF EXISTS segment_number CASCADE;

-- Блок для удаления всех процедур
DROP PROCEDURE IF EXISTS pr_import_table_csv;
DROP PROCEDURE IF EXISTS pr_import_table_tsv;
DROP PROCEDURE IF EXISTS pr_import_table_all_tsv_mini;
DROP PROCEDURE IF EXISTS pr_import_table_all_tsv_full;
DROP PROCEDURE IF EXISTS pr_synchronize_sequences;

--------------------------------------------------------------------------------
-- СОЗДАЁМ БД !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
--------------------------------------------------------------------------------
-- CREATE DATABASE sql3_hubertfu_and_bastionm;

--------------------------------------------------------------------------------
-- Выбираем БД и создаем таблицы
-- \c sql3_hubertfu_and_bastionm
--------------------------------------------------------------------------------

-- Таблица Персональные данные
CREATE TABLE personal_data (
    customer_id SERIAL PRIMARY KEY,
    customer_name VARCHAR NOT NULL,
    customer_surname VARCHAR NOT NULL,
    customer_primary_email VARCHAR NOT NULL,
    customer_primary_phone VARCHAR NOT NULL
);

-- Таблица Карты
CREATE TABLE cards (
    customer_card_id SERIAL PRIMARY KEY,
    customer_id INTEGER NOT NULL,
    CONSTRAINT fk_cards_customer_id FOREIGN KEY (customer_id) REFERENCES personal_data(customer_id)
);

CREATE INDEX IF NOT EXISTS idx_cards_customer_id ON cards USING btree(customer_id);

-- Таблица Группы SKU
CREATE TABLE groups_sku (
    group_id SERIAL PRIMARY KEY,
    group_name VARCHAR NOT NULL
);

-- Таблица Товарная матрица
CREATE TABLE sku (
    sku_id SERIAL PRIMARY KEY,
    sku_name VARCHAR NOT NULL,
    group_id INTEGER NOT NULL,
    CONSTRAINT fk_sku_group_id FOREIGN KEY (group_id) REFERENCES groups_sku(group_id)
);

CREATE INDEX IF NOT EXISTS idx_sku_group_id ON sku USING btree(group_id);

-- Таблица Торговые точки
CREATE TABLE stores (
    transaction_store_id INTEGER NOT NULL,
    sku_id INTEGER NOT NULL,
    sku_purchase_price NUMERIC NOT NULL,
    sku_retail_price NUMERIC NOT NULL,
    CONSTRAINT fk_stores_sku_id FOREIGN KEY (sku_id) REFERENCES sku(sku_id)
);

CREATE INDEX IF NOT EXISTS idx_stores_sku_id ON stores USING btree(sku_id);
CREATE INDEX IF NOT EXISTS idx_stores_transaction_store_id ON stores USING btree(transaction_store_id);

-- Таблица Транзакции
CREATE TABLE transactions (
    transaction_id SERIAL PRIMARY KEY,
    customer_card_id INTEGER NOT NULL,
    transaction_summ NUMERIC NOT NULL,
    transaction_datetime TIMESTAMP DEFAULT NOW(),
    transaction_store_id INTEGER NOT NULL,
    CONSTRAINT fk_transactions_customer_card_id FOREIGN KEY (customer_card_id) REFERENCES cards(customer_card_id)
);

CREATE INDEX IF NOT EXISTS idx_transactions_customer_card_id ON transactions USING btree(customer_card_id);
CREATE INDEX IF NOT EXISTS idx_transactions_transaction_store_id ON transactions USING btree(transaction_store_id);
CREATE INDEX IF NOT EXISTS idx_transactions_transaction_datetime ON transactions USING btree(transaction_datetime);

-- Таблица Чеки
CREATE TABLE checks (
    transaction_id INTEGER NOT NULL,
    sku_id INTEGER NOT NULL,
    sku_amount NUMERIC NOT NULL,
    sku_summ NUMERIC NOT NULL,
    sku_summ_paid NUMERIC NOT NULL,
    sku_discount NUMERIC NOT NULL,
    CONSTRAINT fk_checks_transactions_id FOREIGN KEY (transaction_id) REFERENCES transactions(transaction_id),
    CONSTRAINT fk_checks_sku_id FOREIGN KEY (sku_id) REFERENCES sku(sku_id)
);

CREATE INDEX IF NOT EXISTS idx_checks_transaction_id ON checks USING btree(transaction_id);
CREATE INDEX IF NOT EXISTS idx_checks_sku_id ON checks USING btree(sku_id);

-- Таблица Дата формирования анализа
CREATE TABLE date_of_analysis_formation (analysis_formation TIMESTAMP DEFAULT NOW());

CREATE INDEX IF NOT EXISTS idx_date_of_analysis_formation_analysis_formation ON date_of_analysis_formation USING btree(analysis_formation);

-- Создание таблицы для сегментирования (segment_number) (Ипользуется в Part2_1)
CREATE TABLE segment_number (
    segment SERIAL PRIMARY KEY,
    average_check VARCHAR NOT NULL,
    frequency_of_purchases VARCHAR NOT NULL,
    churn_probability VARCHAR NOT NULL
);

-- Создание таблицы для установления входных данных
-- key_setting_name:
--     наименование: названиеФункции/Процедуры_названиеВходногоПараметра
--     допустимые параметры: mv_groups_flag;
--                           mv_groups_days_interv;
--                           mv_groups_count_transcaions;
-- set_value: значение входного параметра
CREATE TABLE input_settings (
    key_setting_name VARCHAR,
    set_value VARCHAR
);

CREATE UNIQUE INDEX IF NOT EXISTS idx_input_settings_key_setting_name ON input_settings USING btree (key_setting_name);

--------------------------------------------------------------------------------
-- Импорт данных
--------------------------------------------------------------------------------
--
-- Процедура для синхронизации сиквенса таблицы после импорта данных
--
CREATE
OR REPLACE PROCEDURE pr_synchronize_sequences(
    target_table_name TEXT,
    target_table_serial_name TEXT
) AS $$
BEGIN
    EXECUTE 
        FORMAT(
            '
                LOCK TABLE %1$I IN EXCLUSIVE MODE;
                SELECT setval(%2$L, COALESCE((SELECT MAX(%3$I)+1 FROM %4$I), 1), false);
            ',
            target_table_name,
            target_table_name || '_' || target_table_serial_name || '_seq',
            target_table_serial_name,
            target_table_name
        
    );

END;
$$ LANGUAGE plpgsql;

--
-- Процедура для импорта csv-файла с разделителем `delim`, хранящемуся по пути
-- `path` в таблицу `target_table_name`
--
CREATE
OR REPLACE PROCEDURE pr_import_table_csv(
    target_table_name TEXT,
    path TEXT,
    delim VARCHAR(1),
    date_style TEXT DEFAULT 'GERMAN'
) AS $$
BEGIN
    EXECUTE format('SET DATESTYLE TO %1$s', date_style);

    --
    -- $I - обрабатывает значение аргумента как SQL-идентификатор, при необходимости
    -- заключая его в кавычки
    -- $L - заключает значение аргумента в апострофы, как строку SQL
    -- $s форматирует значение аргумента как простую строку.
    -- E' - Константа управляющей строки, включает поддержку в строке
    -- escape-последовательностей, например, '\t'
    --
    -- Если предполагается, что 1я строка содержит заголовки, то надо заменить "CSV"
    -- на "CSV HEADER"
    EXECUTE format(
        'COPY %1$I FROM %2$L WITH DELIMITER E''%3$s'' CSV',
        target_table_name,
        path,
        delim
    );
END;
$$ LANGUAGE plpgsql;

--
-- Процедура для импорта tsv-файла, хранящемуся по пути `path` в таблицу
-- `target_table_name`
--
CREATE
OR REPLACE PROCEDURE pr_import_table_tsv(
    target_table_name TEXT,
    path TEXT,
    date_style TEXT DEFAULT 'GERMAN'
) AS $$
BEGIN
    CALL pr_import_table_csv(target_table_name, path, '\t', date_style);
END;
$$ LANGUAGE plpgsql;

--
-- Процедура для заполнения таблицы для сегментирования (segment_number)
--
CREATE
OR REPLACE PROCEDURE pr_fill_segment_number()
AS $$
DECLARE
    average_check VARCHAR[] := ARRAY['Low', 'Medium', 'High'];
    frequency_of_purchases VARCHAR[] := ARRAY['Low', 'Medium', 'High'];
    churn_probability VARCHAR[] := ARRAY['Low', 'Medium', 'High'];
    num VARCHAR; num_1 VARCHAR; num_2 VARCHAR;
BEGIN
    -- Заполнения таблицы (segment_number)
    FOREACH num IN ARRAY average_check
    LOOP
        FOREACH num_1 IN ARRAY frequency_of_purchases
        LOOP
            FOREACH num_2 IN ARRAY churn_probability
            LOOP
                INSERT INTO segment_number (average_check, frequency_of_purchases, churn_probability)
                VALUES (num, num_1, num_2);
            END LOOP;
        END LOOP;
    END LOOP;
END;
$$ LANGUAGE plpgsql;
--
-- Процедура для импорта всех tsv-файла из materials (Mini)
--
CREATE
OR REPLACE PROCEDURE pr_import_table_all_tsv_mini(
    project_source TEXT,
    date_style TEXT DEFAULT 'GERMAN'
) AS $$
BEGIN
    CALL pr_import_table_tsv(
        'personal_data',
        project_source || 'datasets/Personal_Data_Mini.tsv',
        date_style
    );

    CALL pr_import_table_tsv(
        'cards',
        project_source || 'datasets/Cards_Mini.tsv',
        date_style
    );

    CALL pr_import_table_tsv(
        'groups_sku',
        project_source || 'datasets/Groups_SKU_Mini.tsv',
        date_style
    );

    CALL pr_import_table_tsv(
        'sku',
        project_source || 'datasets/SKU_Mini.tsv',
        date_style
    );

    CALL pr_import_table_tsv(
        'stores',
        project_source || 'datasets/Stores_Mini.tsv',
        date_style
    );

    CALL pr_import_table_tsv(
        'transactions',
        project_source || 'datasets/Transactions_Mini.tsv',
        date_style
    );

    CALL pr_import_table_tsv(
        'checks',
        project_source || 'datasets/Checks_Mini.tsv',
        date_style
    );

    CALL pr_import_table_tsv(
        'date_of_analysis_formation',
        project_source || 'datasets/Date_Of_Analysis_Formation.tsv',
        date_style
    );

    CALL pr_fill_segment_number();
    CALL pr_synchronize_sequences('personal_data', 'customer_id');
    CALL pr_synchronize_sequences('cards', 'customer_card_id');
    CALL pr_synchronize_sequences('groups_sku', 'group_id');
    CALL pr_synchronize_sequences('sku', 'sku_id');
    CALL pr_synchronize_sequences('transactions', 'transaction_id');
END;
$$ LANGUAGE plpgsql;

--
-- Процедура для импорта всех tsv-файла из materials (Mini)
--
CREATE
OR REPLACE PROCEDURE pr_import_table_all_tsv_full(
    project_source TEXT,
    date_style TEXT DEFAULT 'GERMAN'
) AS $$
BEGIN
    CALL pr_import_table_tsv(
        'personal_data',
        project_source || 'datasets/Personal_Data.tsv',
        date_style
    );

    CALL pr_import_table_tsv(
        'cards',
        project_source || 'datasets/Cards.tsv',
        date_style
    );

    CALL pr_import_table_tsv(
        'groups_sku',
        project_source || 'datasets/Groups_SKU.tsv',
        date_style
    );

    CALL pr_import_table_tsv(
        'sku',
        project_source || 'datasets/SKU.tsv',
        date_style
    );

    CALL pr_import_table_tsv(
        'stores',
        project_source || 'datasets/Stores.tsv',
        date_style
    );

    CALL pr_import_table_tsv(
        'transactions',
        project_source || 'datasets/Transactions.tsv',
        date_style
    );

    CALL pr_import_table_tsv(
        'checks',
        project_source || 'datasets/Checks.tsv',
        date_style
    );

    CALL pr_import_table_tsv(
        'date_of_analysis_formation',
        project_source || 'datasets/Date_Of_Analysis_Formation.tsv',
        date_style
    );

    CALL pr_fill_segment_number();
    CALL pr_synchronize_sequences('personal_data', 'customer_id');
    CALL pr_synchronize_sequences('cards', 'customer_card_id');
    CALL pr_synchronize_sequences('groups_sku', 'group_id');
    CALL pr_synchronize_sequences('sku', 'sku_id');
    CALL pr_synchronize_sequences('transactions', 'transaction_id');

END;
$$ LANGUAGE plpgsql;

--
-- Процедура для импорта всех таблиц в csv
--
CREATE
OR REPLACE PROCEDURE pr_import_table_all_csv(
    project_source TEXT,
    delim VARCHAR(1),
    date_style TEXT DEFAULT 'GERMAN'
) AS $$
BEGIN
    CALL pr_import_table_csv(
        'personal_data',
        project_source || 'personal_data.csv',
        delim,
        date_style
    );

    CALL pr_import_table_csv(
        'cards',
        project_source || 'cards.csv',
        delim,
        date_style
    );

    CALL pr_import_table_csv(
        'groups_sku',
        project_source || 'groups_sku.csv',
        delim,
        date_style
    );

    CALL pr_import_table_csv(
        'sku',
        project_source || 'sku.csv',
        delim,
        date_style
    );

    CALL pr_import_table_csv(
        'stores',
        project_source || 'stores.csv',
        delim,
        date_style
    );

    CALL pr_import_table_csv(
        'transactions',
        project_source || 'transactions.csv',
        delim,
        date_style
    );

    CALL pr_import_table_csv(
        'checks',
        project_source || 'checks.csv',
        delim,
        date_style
    );

    CALL pr_import_table_csv(
        'date_of_analysis_formation',
        project_source || 'date_of_analysis_formation.csv',
        delim,
        date_style
    );

    CALL pr_fill_segment_number();
    CALL pr_synchronize_sequences('personal_data', 'customer_id');
    CALL pr_synchronize_sequences('cards', 'customer_card_id');
    CALL pr_synchronize_sequences('groups_sku', 'group_id');
    CALL pr_synchronize_sequences('sku', 'sku_id');
    CALL pr_synchronize_sequences('transactions', 'transaction_id');

END;
$$ LANGUAGE plpgsql;

--------------------------------------------------------------------------------
-- Экспорт данных
--------------------------------------------------------------------------------
--
-- Процедура для экспорта данных таблцицы `target_table_name` в csv с
-- разделителем `delim` хранящемуся по пути `path`.
--
CREATE
OR REPLACE PROCEDURE pr_export_table_csv(
    target_table_name TEXT,
    path TEXT,
    delim VARCHAR(1),
    date_style TEXT DEFAULT 'GERMAN'
) AS $$
BEGIN
    EXECUTE format('SET DATESTYLE TO %1$s', date_style);
    EXECUTE format(
        'COPY (SELECT * FROM %1$I) TO %2$L WITH DELIMITER E''%3$s'' CSV',
        target_table_name,
        path,
        delim
    );
END;
$$ LANGUAGE plpgsql;

--
-- Процедура для экспорта данных таблцицы `target_table_name` в tsv, хранящемуся
-- по пути `path`.
--
CREATE
OR REPLACE PROCEDURE pr_export_table_tsv(
    target_table_name TEXT,
    path TEXT,
    date_style TEXT DEFAULT 'GERMAN'
) AS $$
BEGIN
    CALL pr_export_table_csv(target_table_name, path, '\t', date_style);
END;
$$ LANGUAGE plpgsql;

--
-- Процедура для экспорта всех таблиц в tsv
--
CREATE
OR REPLACE PROCEDURE pr_export_tables_tsv(
    project_source TEXT,
    date_style TEXT DEFAULT 'GERMAN'
) AS $$
BEGIN
    CALL pr_export_table_tsv(
        'personal_data',
        project_source || 'personal_data.tsv',
        date_style
    );

    CALL pr_export_table_tsv(
        'cards',
        project_source || 'cards.tsv',
        date_style
    );

    CALL pr_export_table_tsv(
        'groups_sku',
        project_source || 'groups_sku.tsv',
        date_style
    );

    CALL pr_export_table_tsv(
        'sku',
        project_source || 'sku.tsv',
        date_style
    );

    CALL pr_export_table_tsv(
        'stores',
        project_source || 'stores.tsv',
        date_style
    );

    CALL pr_export_table_tsv(
        'transactions',
        project_source || 'transactions.tsv',
        date_style
    );

    CALL pr_export_table_tsv(
        'checks',
        project_source || 'checks.tsv',
        date_style
    );

    CALL pr_export_table_tsv(
        'date_of_analysis_formation',
        project_source || 'date_of_analysis_formation.tsv',
        date_style
    );
END;
$$ LANGUAGE plpgsql;

--
-- Процедура для экспорта всех таблиц в csv
--
CREATE
OR REPLACE PROCEDURE pr_export_tables_csv(
    project_source TEXT,
    delim VARCHAR(1),
    date_style TEXT DEFAULT 'GERMAN'
) AS $$
BEGIN
    CALL pr_export_table_csv(
        'personal_data',
        project_source || 'personal_data.csv',
        delim,
        date_style
    );

    CALL pr_export_table_csv(
        'cards',
        project_source || 'cards.csv',
        delim,
        date_style
    );

    CALL pr_export_table_csv(
        'groups_sku',
        project_source || 'groups_sku.csv',
        delim,
        date_style
    );

    CALL pr_export_table_csv(
        'sku',
        project_source || 'sku.csv',
        delim,
        date_style
    );

    CALL pr_export_table_csv(
        'stores',
        project_source || 'stores.csv',
        delim,
        date_style
    );

    CALL pr_export_table_csv(
        'transactions',
        project_source || 'transactions.csv',
        delim,
        date_style
    );

    CALL pr_export_table_csv(
        'checks',
        project_source || 'checks.csv',
        delim,
        date_style
    );

    CALL pr_export_table_csv(
        'date_of_analysis_formation',
        project_source || 'date_of_analysis_formation.csv',
        delim,
        date_style
    );
END;
$$ LANGUAGE plpgsql;

--------------------------------------------------------------------------------
-- Проверяем работу процедур
--------------------------------------------------------------------------------

-- Создаем глобальную переменную c адресом проекта на текущем компьютере
SET path_to_project.var TO '/Users/hubertfu/projects/main/sql/SQL3_RetailAnalitycs_v1.0-0/';

-- Импортировать tsv из materials (mini)
CALL pr_import_table_all_tsv_mini(current_setting('path_to_project.var'));

-- Импортировать tsv из materials (maxi)
-- CALL pr_import_table_all_tsv_full(current_setting('path_to_project.var'));

-- Импортировать csv из import
-- CALL pr_import_table_all_csv(current_setting('path_to_project.var') || 'src/import/', ',');

-- Экспортировать все таблицы в tsv в папку 'src/export/'
CALL pr_export_tables_tsv(current_setting('path_to_project.var') || 'src/export/');

-- Экспортировать все таблицы в tsv в папку 'src/export/'
CALL pr_export_tables_csv(current_setting('path_to_project.var') || 'src/export/', ',');