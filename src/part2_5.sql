--------------------------------------------------------------------------------
-- Part 2. Создание представлений
--------------------------------------------------------------------------------
-- Дополнительные представления и процедуры, которые не требуются по заданию, но
-- необходимы для нашей реализации
--------------------------------------------------------------------------------
--
--------------------------------------------------------------------------------
-- Функция для получения данных для формирования представления 
-- mv_sku_transactions_share (расчет максимальной доли SKU в группе)
--------------------------------------------------------------------------------
--
CREATE OR REPLACE FUNCTION fnc_create_mv_sku_transactions_share() RETURNS TABLE (
    sku_id INTEGER,
    group_id INTEGER,
    share_sku_in_group NUMERIC
) AS $$
DECLARE
    first_date_ TIMESTAMP := fnc_first_date();
    last_date_ TIMESTAMP := fnc_last_date();
BEGIN
    RETURN QUERY
        SELECT
            sku.sku_id AS sku_id,
            sku.group_id AS group_id,
            COUNT(transactions.transaction_id) :: NUMERIC / SUM(COUNT(transactions.transaction_id)) OVER (PARTITION BY sku.group_id) AS share_sku_in_group
        FROM
            transactions
            JOIN cards ON cards.customer_card_id = transactions.customer_card_id
            JOIN checks ON checks.transaction_id = transactions.transaction_id
            JOIN sku ON sku.sku_id = checks.sku_id
        WHERE 
            transactions.transaction_datetime BETWEEN first_date_ AND last_date_
        GROUP BY
            sku.sku_id,
            sku.group_id;

END;
$$ LANGUAGE plpgsql;

--------------------------------------------------------------------------------
-- Процедура для создания представления mv_sku_transactions_share
--------------------------------------------------------------------------------
--
CREATE
OR REPLACE PROCEDURE pr_create_mv_sku_transactions_share() AS $$
BEGIN
    -- Пересоздание представления mv_sku_transactions_share
    DROP MATERIALIZED VIEW IF EXISTS mv_sku_transactions_share;
    CREATE MATERIALIZED VIEW mv_sku_transactions_share AS
        SELECT
            sku_id,
            group_id,
            share_sku_in_group
        FROM
            fnc_create_mv_sku_transactions_share();
    
    CREATE INDEX IF NOT EXISTS idx_mv_sku_transactions_share_sku_id ON mv_sku_transactions_share USING btree(sku_id);

END;
$$ LANGUAGE plpgsql;

--------------------------------------------------------------------------------
-- Процедура для обновления всех mv
--------------------------------------------------------------------------------
--
CREATE
OR REPLACE PROCEDURE pr_refresh_all_mv() AS $$
BEGIN
    CALL pr_create_mv_customers();
    CALL pr_create_mv_purchase_history();
    CALL pr_create_mv_periods();
    CALL pr_create_mv_groups();
    CALL pr_create_mv_sku_transactions_share();

END;
$$ LANGUAGE plpgsql;

--------------------------------------------------------------------------------
-- Функция для триггера на изменение таблицы date_of_analysis_formation
--------------------------------------------------------------------------------
--
CREATE
OR REPLACE FUNCTION fnc_trg_date_of_analysis_formation() RETURNS TRIGGER AS $$
BEGIN
    CALL pr_refresh_all_mv();
    RETURN NULL;

END;
$$ LANGUAGE plpgsql;

--------------------------------------------------------------------------------
-- Создаем триггер на изменение таблицы date_of_analysis_formation
--------------------------------------------------------------------------------
--
CREATE
OR REPLACE TRIGGER trg_person_audit
AFTER INSERT OR UPDATE OR DELETE OR TRUNCATE ON date_of_analysis_formation
    FOR EACH STATEMENT EXECUTE FUNCTION fnc_trg_date_of_analysis_formation();

--------------------------------------------------------------------------------
-- Создаем представление mv_sku_transactions_share
--------------------------------------------------------------------------------
--
CALL pr_create_mv_sku_transactions_share();