--------------------------------------------------------------------------------
-- Part 5. Формирование персональных предложений, ориентированных на рост частоты визитов
--------------------------------------------------------------------------------
-- Написать функцию, определяющую предложения, ориентированные на кросс-продажи (рост маржи)
-------------------------------------------------------------------------------
CREATE
OR REPLACE FUNCTION fnc_personal_offers_from_increase_frequency_of_visits(
    IN first_date TIMESTAMP,
    IN last_date TIMESTAMP,
    IN count_transaction INTEGER,
    IN max_churn_rate FLOAT,
    IN max_transaction_share FLOAT,
    IN margin_share FLOAT
) RETURNS TABLE (
    customer_id INTEGER,
    start_date TIMESTAMP,
    end_date TIMESTAMP,
    required_transactions_count INTEGER,
    group_name VARCHAR,
    offer_discount_depth INTEGER
) AS $$
BEGIN
    RETURN QUERY
        SELECT
            DISTINCT ON (vg.customer_id) vg.customer_id,
            first_date,
            last_date,
            (
                EXTRACT(
                    EPOCH
                    FROM
                        last_date - first_date
                ) / 86400.0 / customer_frequency
            ) :: INTEGER + count_transaction AS required_transaction_count,
            groups_sku.group_name AS group_name,
            (100 * CEIL(group_minimum_discount / 0.05) * 0.05) :: INTEGER AS offer_discount_depth
        FROM
            mv_groups vg
            JOIN mv_periods vp ON vg.customer_id = vp.customer_id
            AND vg.group_id = vp.group_id
            JOIN groups_sku ON groups_sku.group_id = vp.group_id
            JOIN mv_customers vc ON vc.customer_id = vp.customer_id
            JOIN (
                SELECT
                    mv_purchase_history.customer_id,
                    mv_purchase_history.group_id,
                    SUM(mv_purchase_history.group_cost) AS sum_group_cost,
                    SUM(mv_purchase_history.group_summ) AS sum_group_summ
                FROM
                    mv_purchase_history
                GROUP BY
                    mv_purchase_history.customer_id,
                    mv_purchase_history.group_id
            ) AS vph ON vp.customer_id = vph.customer_id
            AND vp.group_id = vph.group_id
        WHERE
            group_churn_rate <= max_churn_rate
            AND group_discount_share < max_transaction_share
            AND CEIL(group_minimum_discount / 0.05) * 0.05 < (
                margin_share / 100 * (
                    (sum_group_summ - sum_group_cost) / sum_group_cost
                )
            )
        ORDER BY
            vg.customer_id,
            vg.group_affinity_index DESC,
            vg.group_id;

END;
$$ LANGUAGE plpgsql;

--------------------------------------------------------------------------------
-- Проверка функции
--------------------------------------------------------------------------------
SELECT
    *
FROM
    fnc_personal_offers_from_increase_frequency_of_visits(
        '2022-08-18T21:17:43' :: TIMESTAMP,
        '2022-08-18T21:17:43' :: TIMESTAMP,
        1,
        1.1,
        0.58,
        50.0
    );

SELECT
    *
FROM
    fnc_personal_offers_from_increase_frequency_of_visits(
        '2022-08-18T21:17:43' :: TIMESTAMP,
        '2022-10-18T21:17:43' :: TIMESTAMP,
        50,
        100,
        100,
        100
    );