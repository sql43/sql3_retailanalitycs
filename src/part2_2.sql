--------------------------------------------------------------------------------
-- Part 2. Создание представлений
--------------------------------------------------------------------------------
-- Представление История покупок
--------------------------------------------------------------------------------
--
--
--------------------------------------------------------------------------------
-- Процедура для создания представления mv_purchase_history
--------------------------------------------------------------------------------
--
CREATE
OR REPLACE PROCEDURE pr_create_mv_purchase_history() AS $$
DECLARE
    first_date_ TIMESTAMP := fnc_first_date();
    last_date_ TIMESTAMP := fnc_last_date();
BEGIN
    DROP MATERIALIZED VIEW IF EXISTS mv_purchase_history;
    EXECUTE FORMAT(
        '
        CREATE MATERIALIZED VIEW mv_purchase_history AS
                SELECT
                    cards.customer_id AS Customer_ID,
                    transactions.transaction_id AS Transaction_ID,
                    transactions.transaction_datetime AS Transaction_DateTime,
                    sku.group_id AS Group_ID,
                    SUM(stores.sku_purchase_price * checks.sku_amount) AS Group_Cost,
                    SUM(checks.sku_summ) AS Group_Summ,
                    SUM(checks.sku_summ_paid) AS Group_Summ_Paid
                FROM
                    transactions
                    JOIN cards ON cards.customer_card_id = transactions.customer_card_id
                    JOIN checks ON checks.transaction_id = transactions.transaction_id
                    JOIN sku ON sku.sku_id = checks.sku_id
                    JOIN stores ON stores.sku_id = sku.sku_id
                    AND transactions.transaction_store_id = stores.transaction_store_id
                WHERE transactions.transaction_datetime BETWEEN %L AND %L
                GROUP BY
                    cards.customer_id,
                    transactions.transaction_id,
                    sku.group_id;
        ',
        first_date_,
        last_date_
    );
    -- Индекс пригодится, если при генерации других view ограничения по дате сильно будут сокращать выборку
    CREATE INDEX IF NOT EXISTS idx_mv_purchase_history_transaction_datetime ON mv_purchase_history USING btree(transaction_datetime);
    -- Индексы для сортировки mv при join
    CREATE INDEX IF NOT EXISTS idx_mv_purchase_history_complex1 ON mv_purchase_history USING btree(customer_id, group_id, transaction_id);
    CREATE INDEX IF NOT EXISTS idx_mv_purchase_history_complex2 ON mv_purchase_history USING btree(customer_id, group_id);

END;
$$ LANGUAGE plpgsql;

--------------------------------------------------------------------------------
-- создаем представление mv_customers
--------------------------------------------------------------------------------
--
CALL pr_create_mv_purchase_history();

--------------------------------------------------------------------------------
-- Проверка view
--------------------------------------------------------------------------------
--
-- Смотрим все данные mv
SELECT
    *
FROM
    mv_purchase_history;
	
-- Смотрим записи по покупателю с id = 3
SELECT
    *
FROM
    mv_purchase_history
WHERE
    customer_id = 3;

-- Смотрим только топ100 записей
SELECT
    *
FROM
    mv_purchase_history
LIMIT
    100;

-- Смотрим только записи за определенный период
SELECT
    *
FROM
    mv_purchase_history
WHERE
    transaction_datetime > '2022-01-01 00:00:00'
    AND transaction_datetime < '2022-03-01 00:00:00';