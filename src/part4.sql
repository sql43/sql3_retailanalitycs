--------------------------------------------------------------------------------
-- Функция для расчета среднего чека методикой расчета по периоду
--------------------------------------------------------------------------------
-- Отбираем все записи из таблицы transactions за указанный период для
-- конкретного пользователя и считаем среднее значение для суммы транзакции
--
CREATE
OR REPLACE FUNCTION fnc_part4_get_average_check_by_dates(
    -- ID покупателя
    _customer_id INTEGER,
    -- первая дата периода
    _first_date TIMESTAMP,
    -- последняя дата периода
    _last_date TIMESTAMP
) RETURNS NUMERIC AS $$
DECLARE
    result NUMERIC;
BEGIN
    SELECT
        AVG(transaction_summ) AS average_check_sum INTO result
    FROM
        transactions
        JOIN cards ON cards.customer_card_id = transactions.customer_card_id
    WHERE
        cards.customer_id = _customer_id
        AND transactions.transaction_datetime >= _first_date
        AND transactions.transaction_datetime <= _last_date;

    RETURN result;

END;
$$ LANGUAGE plpgsql;

--------------------------------------------------------------------------------
-- Функция для расчета среднего чека методикой по количеству _transactions_count 
-- последних транзакций
--------------------------------------------------------------------------------
-- Отбираем _transactions_count записей из таблицы transactions для
-- конкретного пользователя и считаем среднее значение для суммы транзакции
--
CREATE
OR REPLACE FUNCTION fnc_part4_get_average_check_by_counts(
    -- ID покупателя
    _customer_id INTEGER,
    -- количество транзакций (для 2 метода)
    _transactions_count INTEGER
) RETURNS NUMERIC AS $$
DECLARE
    result NUMERIC;
BEGIN
    SELECT
        AVG(transaction_summ) AS average_check_sum INTO result
    FROM
        (
            SELECT
                transactions.transaction_summ AS transaction_summ,
                ROW_NUMBER() OVER (
                    PARTITION BY cards.customer_id
                    ORDER BY
                        transactions.transaction_datetime DESC
                ) AS transactions_num
            FROM
                transactions
                JOIN cards ON cards.customer_card_id = transactions.customer_card_id
            WHERE
                cards.customer_id = _customer_id
        ) AS tmp
    WHERE
        transactions_num <= _transactions_count;

    RETURN result;

END;
$$ LANGUAGE plpgsql;

--------------------------------------------------------------------------------
-- Функция, определяющая предложения, ориентированные на рост среднего чека
--------------------------------------------------------------------------------
--
CREATE
OR REPLACE FUNCTION fnc_part4(
    -- метод расчета среднего чека (1 - за период, 2 - за количество)
    _average_check_calculation_method INTEGER DEFAULT 1,
    -- первая дата периода (для 1 метода)
    _first_date TIMESTAMP DEFAULT NULL,
    -- последняя дата периода (для 1 метода)
    _last_date TIMESTAMP DEFAULT NULL,
    -- количество транзакций (для 2 метода)
    _transactions_count INTEGER DEFAULT 0,
    -- коэффициент увеличения среднего чека
    _average_check_increase_factor NUMERIC DEFAULT 1,
    -- максимальный индекс оттока
    _maximum_churn_index NUMERIC DEFAULT 0,
    -- максимальная доля транзакций со скидкой (в процентах)
    _maximum_share_of_discounted_transactions NUMERIC DEFAULT 0,
    -- допустимая доля маржи (в процентах)
    _allowable_margin_share NUMERIC DEFAULT 0
) RETURNS TABLE (
    Customer_ID INTEGER,
    Required_Check_Measure NUMERIC,
    Group_Name VARCHAR,
    Offer_Discount_Depth NUMERIC
) AS $$
DECLARE
    analysis_first_date TIMESTAMP = fnc_first_date();
    analysis_last_date TIMESTAMP = fnc_last_date();
BEGIN
    -- Сначала проверяем входные параметры функции на корректность и бросаем
    -- исключения при необходимости
    --
    -- Допустимы только методы 1 и 2
    IF (_average_check_calculation_method NOT IN (1, 2)) THEN
        RAISE EXCEPTION 'fnc_part4: Unknown calculation method';
    END IF;

    -- Для метода 1 даты должны иметь корректное значение и дата начала должна
    -- быть меньше даты конца
    IF (
        _average_check_calculation_method = 1
        AND (
            _first_date IS NULL
            OR _last_date IS NULL
            OR _first_date >= _last_date
        )
    ) THEN
        RAISE EXCEPTION 'fnc_part4: Incorrect dates';
    END IF;
    
    -- Для метода 2 количество транзакций должно быть больше 0
    IF (
        _average_check_calculation_method = 2
        AND _transactions_count < 1
    ) THEN
        RAISE EXCEPTION 'fnc_part4: transactions count must be greater then 0';
    END IF;

    -- Валидируем "Максимальная доля транзакций со скидкой (в процентах)"
    IF (_maximum_share_of_discounted_transactions < 0 OR _maximum_share_of_discounted_transactions > 100) THEN
        RAISE EXCEPTION 'fnc_part6: maximum share of discounted transactions must be in [0,100]';
    END IF;

    -- Валидируем "Допустимая доля маржи (в процентах)"
    IF (_allowable_margin_share < 0 OR _allowable_margin_share > 100) THEN
        RAISE EXCEPTION 'fnc_part6: allowable margin share must be in [0,100]';
    END IF;

    -- Указанный период должен быть внутри общего анализируемого периода. В
    -- случае указания слишком ранней или слишком поздней даты система
    -- автоматически подставляет дату, соответственно, начала или окончания
    -- анализируемого периода.
    IF (analysis_first_date > _first_date) THEN
        _first_date = analysis_first_date;
    END IF;
    IF (analysis_last_date < _last_date) THEN
        _last_date = analysis_last_date;
    END IF;

    -- Переводим "Максимальная доля транзакций со скидкой (в процентах)" и 
    -- "Допустимая доля маржи (в процентах)" из процентов в доли числа
    _maximum_share_of_discounted_transactions = _maximum_share_of_discounted_transactions / 100;
    _allowable_margin_share = _allowable_margin_share / 100;

    RETURN QUERY
        -- Создаем вспомогательную CTE, где для каждой пары customer_id-group_id
        -- считаем максимально возможную скидку для группы, как
        -- (допустимая доля маржи) * (средняя маржа по группе в процентах)
        WITH cte_with_max_discount AS (
            SELECT
                mv_purchase_history.customer_id,
                mv_purchase_history.group_id,
                _allowable_margin_share * SUM(Group_Summ - Group_Cost) / SUM(Group_Summ) AS max_discount
            FROM
                mv_purchase_history
            GROUP BY
                mv_purchase_history.customer_id,
                mv_purchase_history.group_id
        ),
        -- Создаем вспомогательную CTE, где оставляем только группы, подходящие
        -- по условию отбора, нумеруем их в group_number в разрезе каждого
        -- покупателя, считаем минимльную скидку, кратную 5% и округленную вверх
        cte_result AS (
            SELECT
                ROW_NUMBER() OVER (
                    PARTITION BY mv_groups.customer_id
                    ORDER BY
                        Group_Affinity_Index DESC, -- Индекс востребованности группы – максимальный из всех возможных.
                        groups_sku.group_id -- На случай равенства индекса задаем второе условие, чтобы гарантировать стабильный результ
                ) AS group_number,
                mv_groups.customer_id,
                groups_sku.group_name,
                CEIL(Group_Minimum_Discount / 0.05) * 0.05 AS group_minimum_discount_round
            FROM
                mv_groups
                JOIN cte_with_max_discount ON cte_with_max_discount.customer_id = mv_groups.customer_id
                AND cte_with_max_discount.group_id = mv_groups.group_id
                JOIN groups_sku ON groups_sku.group_id = mv_groups.group_id
            WHERE
                mv_groups.Group_Minimum_Discount > 0
                AND mv_groups.Group_Churn_Rate <= _maximum_churn_index -- Индекс оттока по данной группе не должен превышать заданного пользователем значения.
                AND mv_groups.Group_Discount_Share < _maximum_share_of_discounted_transactions -- Доля транзакций со скидкой по данной группе – менее заданного пользователем значения. 
                AND (CEIL(Group_Minimum_Discount / 0.05) * 0.05) < cte_with_max_discount.max_discount -- максимально допустимый размер скидки для вознаграждения должен быть больше текущей минимальной скидки
        )
        -- Формируем итоговый результат из вспомогательных CTE
        SELECT
            -- ID покупателя берем как есть
            cte_result.customer_id AS Customer_ID,
            -- Целевое значение среднего чека считаем по выбранному методу,
            -- используя соответствующую функцию для получения среднего чека и
            -- умножаем на коэффициент увеличения среднего чека
            CASE
                WHEN _average_check_calculation_method = 1 THEN _average_check_increase_factor * fnc_part4_get_average_check_by_dates(
                    cte_result.customer_id,
                    _first_date,
                    _last_date
                )
                ELSE _average_check_increase_factor * fnc_part4_get_average_check_by_counts(cte_result.customer_id, _transactions_count)
            END AS Required_Check_Measure,
            -- Имя группы берем как есть
            cte_result.group_name AS Group_Name,
            -- Итоговую скидку переводим в проценты из долей числа
            ROUND(100 * group_minimum_discount_round) AS Offer_Discount_Depth
        FROM
            cte_result
        WHERE
            -- Отбираем одну группу для каждого покупателя (первую после всех
            -- заданных сортировок)
            cte_result.group_number = 1;
END;
$$ LANGUAGE plpgsql;

--------------------------------------------------------------------------------
-- Проверка функции
--------------------------------------------------------------------------------
-- Пример 1
SELECT
    *
FROM
    fnc_part4(
        _average_check_calculation_method := 1,
        _first_date := '2000-01-01 00:00:00',
        _last_date := '2023-12-31 23:59:59',
        _average_check_increase_factor := 1.15,
        _maximum_churn_index := 1.1,
        _maximum_share_of_discounted_transactions := 67,
        _allowable_margin_share := 70
    );

-- Пример 2
SELECT
    *
FROM
    fnc_part4(
        _average_check_calculation_method := 2,
        _transactions_count := 1000,
        _average_check_increase_factor := 1.15,
        _maximum_churn_index := 5,
        _maximum_share_of_discounted_transactions := 55,
        _allowable_margin_share := 50
    );

-- Пример 3
SELECT
    *
FROM
    fnc_part4(
        _average_check_calculation_method := 1,
        _first_date := '2000-01-01 00:00:00',
        _last_date := '2023-12-31 23:59:59',
        _average_check_increase_factor := 1.5,
        _maximum_churn_index := 10,
        _maximum_share_of_discounted_transactions := 80,
        _allowable_margin_share := 80
    );
